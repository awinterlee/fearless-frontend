window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    const response = await fetch(url);
    console.error(response)
    console.log(response)

    if (response.ok) {
      const data = await response.json();
      console.log(data)
      console.error(data)

      const selectTag = document.getElementById('conference')
      for (const conference of data.conferences) {
        const option = document.createElement('option')
        option.value = conference.id
        option.innerHTML = conference.name
        selectTag.appendChild(option)
      }
        const formTag = document.getElementById('create-presentation-form')
        formTag.addEventListener('submit', async event => {
            event.preventDefault()
            const formData = new FormData(formTag)
            const json = JSON.stringify(Object.fromEntries(formData))

            const locationUrl = "conferences/<int:conference_id>/presentations/"
            const fetchConfig = {
                method: 'post',
                body: json,
                headers: {
                    'Content-Type': 'application/json',
                }
            }
            const response = await fetch(locationUrl, fetchConfig)
            if (response.ok) {
                formTag.reset()
                const newLocation = await response.json()
                console.log(newLocation)
            }
        })

    }
});
