window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingSpinner = document.getElementById('loading-conference-spinner')
    const successAlert = document.getElementById('success-message')

    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      console.log(data)

      for (let conference of data.conferences) {
        const option = document.createElement('option');
        option.value = conference.href;
        option.innerHTML = conference.name;
        selectTag.appendChild(option);
      }

      selectTag.classList.remove('d-none')
      loadingSpinner.classList.add('d-none')

      const formTag = document.getElementById('create-attendee-form')
        formTag.addEventListener('submit', async event => {
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        console.log(json)

        const locationUrl = 'http://localhost:8001/api/attendees/'
        const fetchConfig = {
            method: 'post',
            body: json,
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(locationUrl, fetchConfig)
            if (response.ok) {
                formTag.classList.add('d-none')
                successAlert.classList.remove('d-none')
            }

    })

    }



  });
