  function createCard(name, description, pictureUrl, startDate, endDate, location) {
    return `
      <div class="col-md-4 mb-4">
      <div class="card" style="box-shadow: 6px 5px 10px; flex: 1 1 300px; margin: 10px;">
        <img src="${pictureUrl}" class="card-img-top" style="max-width: 200px; height: 300px;">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">${startDate} to ${endDate}</div>
      </div>
    `;
  }


window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        console.log("something broke...")
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts).toLocaleDateString();
            const endDate = new Date(details.conference.ends).toLocaleDateString();
            const location = details.conference.location.name
            const html = createCard(name, description, pictureUrl, startDate, endDate, location);
            const row = document.querySelector('.row');
            row.innerHTML += html;
            console.log(details)
          }
        }

      }
    } catch (error) {
        console.error('Houston, we have a problem by Luke', error)
    }

  });
